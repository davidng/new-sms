/**
 * The contents of this file are subject to the OpenMRS Public License
 * Version 1.0 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://license.openmrs.org
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * Copyright (C) OpenMRS, LLC.  All Rights Reserved.
 */
package org.openmrs.module.sms.extension.html;

import java.util.LinkedHashMap;
import java.util.Map;

import org.openmrs.api.context.Context;
import org.openmrs.module.Extension;
import org.openmrs.module.web.extension.AdministrationSectionExt;

/**
 * This class defines the links that will appear on the administration page
 * under the "sms.title" heading.
 */
public class AdminList extends AdministrationSectionExt {
	private static String requiredPrivileges = "View Test SMS,View SMS";

	/**
	 * @see AdministrationSectionExt#getMediaType()
	 */
	public Extension.MEDIA_TYPE getMediaType() {
		return Extension.MEDIA_TYPE.html;
	}

	/**
	 * @see AdministrationSectionExt#getTitle()
	 */
	public String getTitle() {
		return "sms.title";
	}

	@Override
	public String getRequiredPrivilege() {
		if (requiredPrivileges == null) {
			StringBuilder builder = new StringBuilder();
			requiredPrivileges = builder.toString();
		}

		return requiredPrivileges;
		// return "";
	}

	/**
	 * @see AdministrationSectionExt#getLinks()
	 */
	public Map<String, String> getLinks() {
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
		if (Context.hasPrivilege("View Test SMS")) {
			map.put("module/sms/message-test.list", "sms.test_message");
		}
		if (Context.hasPrivilege("View SMS")) {
			map.put("module/sms/message-list.list", "sms.message_list_programmed");
			map.put("module/sms/message-create.list", "sms.create_message");
			map.put("module/sms/gateways.list", "sms.manage_gateway");
			map.put("module/sms/message-queue.list", "sms.message_queue");
		}

		return map;
	}

}
