package org.openmrs.module.sms.web.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Concept;
import org.openmrs.Patient;
import org.openmrs.api.ConceptService;
import org.openmrs.api.context.Context;
import org.openmrs.module.reporting.cohort.definition.CohortDefinition;
import org.openmrs.module.reporting.cohort.definition.service.CohortDefinitionService;
import org.openmrs.module.sms.SmsProgramming;
import org.openmrs.module.sms.SmsProvider;
import org.openmrs.module.sms.SmsQueue;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.send.SendSms;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SMSController {
	protected final Log log = LogFactory.getLog(getClass());
	
	private SmsService service;
	private CohortDefinitionService cohortService;
	private ConceptService conceptService;
	
	@RequestMapping(value="/module/sms/message-create", method=RequestMethod.GET)
	public String createMessage(ModelMap model, HttpServletRequest request)	{
		getSmsService();
		model.addAttribute("cohortList", cohortService.getAllDefinitions(false));
		model.addAttribute("conceptList", conceptService.getAllConcepts());
		return "/module/sms/message-create";		
	}
	
	@RequestMapping(value = "/module/sms/message-create", method = RequestMethod.POST)
	public String doCreateMessage(@RequestParam String name,@RequestParam String text, @RequestParam int days_after,
			@RequestParam String sql,@RequestParam String event, @RequestParam int cohort,@RequestParam int concept, ModelMap model) throws Exception {		
		SmsProgramming smsprog = new SmsProgramming();
		smsprog.setName(name);
		smsprog.setText(text);
		smsprog.setDelay(days_after);
		smsprog.setEvent(event);
		smsprog.setEventSQL(sql);
		smsprog.setCohort(cohort);
		smsprog.setConcept(concept);		
		service.createMessage(smsprog);
		return "redirect:message-create.list";
	}
	
	
	@RequestMapping(value="/module/sms/message-list", method=RequestMethod.GET)
	public String getMessageList(ModelMap model, HttpServletRequest request)	{
		getSmsService();
		List<SmsProgramming> listMessage = service.getAllMessages();
		model.addAttribute("messageList", listMessage);
		return "/module/sms/message-list";		
	}
	
	@RequestMapping(value = "/module/sms/message-list", method = RequestMethod.POST)
	public String doEditMessage(@RequestParam int msgId, @RequestParam String msgName,@RequestParam String msgText, @RequestParam int msgDelay,
			@RequestParam String msgEvent, @RequestParam int msgCohort, ModelMap model) throws Exception {
		SmsProgramming programming = service.getMessageById(msgId);
		if(programming != null){
			programming.setName(msgName);
			programming.setDelay(msgDelay);
			programming.setText(msgText);
			programming.setCohort(msgCohort);
			programming.setEvent(msgEvent);
			service.createMessage(programming);			
		}
		return "redirect:message-list.list";
	}
	
	@RequestMapping(value="/module/sms/message-queue", method=RequestMethod.GET)
	public String getMessageQueue(ModelMap model, HttpServletRequest request)	{
		getSmsService();
		List<SmsQueue> listMessageQueue = service.getAllSmsQueue();
		model.addAttribute("messageQueue", listMessageQueue);
		return "/module/sms/message-queue";		
	}
	
	@RequestMapping(value="/module/sms/gateways", method=RequestMethod.GET)
	public String viewGateways(ModelMap model, HttpServletRequest request)	{
		getSmsService();
		List<SmsProvider> providerList = service.getAllProviders();
		model.addAttribute("providerList", providerList);
		return "/module/sms/gateways";		
	}
	
	@RequestMapping(value = "/module/sms/gateways", method = RequestMethod.POST)
	public String updateGateway(@RequestParam String gateway_id,@RequestParam String name,
			@RequestParam String gateway_user,@RequestParam String gateway_password, @RequestParam String url,
			@RequestParam(required = false) boolean preferred, 
			@RequestParam(required = false) boolean voided, ModelMap model) throws Exception {
		SmsProvider provider = new SmsProvider();
		Integer id = null;
		try{
			id = Integer.parseInt(gateway_id);
		}catch(Exception e){}
		
		provider.setId(id);
		provider.setName(name);
		provider.setUsername(gateway_user);
		provider.setPassword(gateway_password);
		provider.setAddress(url);
		provider.setPreferred(preferred);
		provider.setVoided(voided);		
		service.createProvider(provider);
		
		return "redirect:gateways.list";
	}
	
	@RequestMapping(value="/module/sms/message-test", method=RequestMethod.GET)
	public String testSms(ModelMap model, HttpServletRequest request)	{
		getSmsService();
		List<SmsProgramming> smsList = service.getAllMessages();
		model.addAttribute("smsList", smsList);
		return "/module/sms/message-test";		
	}
	
	@RequestMapping(value = "/module/sms/message-test", method = RequestMethod.POST)
	public String doTestSms(@RequestParam int patientId,@RequestParam int smsId, ModelMap model) throws Exception {
		SendSms.sendMessage(smsId, patientId);
		return "redirect:message-test.list";
	}
		
	private void getSmsService(){
		if(service == null)
			service = (SmsService) Context.getService(SmsService.class);
		if(cohortService == null)
			cohortService = Context.getService(CohortDefinitionService.class);
		if(conceptService == null)
			conceptService =  Context.getConceptService();
	}

}
