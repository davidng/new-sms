<%@ include file="/WEB-INF/template/include.jsp" %>

<%@ page import = "org.openmrs.module.sms.SmsProgramming, java.util.Map, java.util.HashMap" %>

<openmrs:require privilege="View SMS" otherwise="/login.htm" redirect="/module/sms/message-test.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<div>
	<b class="boxHeader">
		<spring:message code="sms.test_message" />
	</b>
	<div class="box">
		<form method="post">
			<table>
				<tr>
					<td>
						<spring:message code="sms.patient.name"/> &nbsp; : &nbsp;
					</td>
					<td>
						<openmrs_tag:patientField formFieldName="patientId" />
					</td>
				</tr>
				<tr>
					<td>
						<spring:message code="sms.sms.name"/> &nbsp; : &nbsp;
					</td>
					<td>
						<select name="smsId">
							<c:forEach items="${smsList}" var="sms">
								<option value="${sms.id}">${sms.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>
			
			<input type="submit" value="<spring:message code="sms.send"/>"/>
		</form>
	</div>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>