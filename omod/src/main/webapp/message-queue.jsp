<%@ include file="/WEB-INF/template/include.jsp" %>

<%@ page import = "org.openmrs.module.sms.SmsProgramming, java.util.Map, java.util.HashMap" %>

<openmrs:require privilege="View SMS" otherwise="/login.htm" redirect="/module/sms/message-queue.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<c:set var="messageQueueSize" value="${fn:length(messageQueue)}" />

<div>
	<b class="boxHeader">
		<spring:message code="sms.message_queue" />
	</b>
	<c:choose>
		<c:when test="${messageQueueSize < 1}">
			<br/>&nbsp;&nbsp;<i>(<spring:message code="sms.null"/>)</i><br/>
		</c:when>
		<c:otherwise>
			<div class="box">
				<form method="post">
					<table cellpadding="4">
						<tr>
							<th><u><spring:message code="sms.id"/></u></th>
							<th><u><spring:message code="sms.sms.name"/></u></th>
							<th><u><spring:message code="sms.patient.name"/></u></th>
							<th><u><spring:message code="sms.no.of.attempt"/></u></th>
							<th><u><spring:message code="sms.last.attempt.datetime"/></u></th>							
						</tr>
						<c:forEach items="${messageQueue}" var="msg" varStatus="varStatus">
							<tr class="<c:choose><c:when test="${varStatus.index % 2 == 0}">evenRow</c:when><c:otherwise>oddRow</c:otherwise></c:choose>">
								<td>${msg.id}</td>
								<td>${msg.programming.name}</td>
								<td><a href="${pageContext.request.contextPath}/patientDashboard.form?patientId=${msg.patient.id}">${msg.patient.personName.fullName}</a></td>
								<td>${msg.numberAttempt}</td>
								<td><openmrs:formatDate date="${msg.lastAttemptDateTime}" type="long" /></td>
							</tr>
						</c:forEach>
					</table>
				</form>
			</div>
		</c:otherwise>
	</c:choose>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>