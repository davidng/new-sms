<%@ include file="/WEB-INF/template/include.jsp" %>

<%@ page import = "org.openmrs.module.sms.SmsProgramming, java.util.Map, java.util.HashMap" %>

<openmrs:require privilege="View SMS" otherwise="/login.htm" redirect="/module/sms/gateways.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<style type="text/css">
	#div1 {
		width: 600px;
		height: 450px;
		padding: 10px;
		border: 1px solid #aaaaaa;
		float:left;
		clear: left;
		overflow: auto;
	}
	#div2 {
		width: 400px;
		height: 450px;
		padding: 10px;
		border: 1px solid #aaaaaa;
		float:left;
		overflow: auto;
	}
</style>

<script type="text/javascript">
	function selectRow(id,name,username,password,address,preferred,voided){
		document.getElementById('gateway_id').value = id;
		document.getElementById('name').value = name;
		document.getElementById('gateway_user').value = username;
		document.getElementById('gateway_password').value = password;
		document.getElementById('url').value = address;
		document.getElementById('preferred').checked = preferred;
		document.getElementById('voided').checked = voided;
	}
	function newGateway(){		
		document.getElementById('gateway_id').value = '';
		document.getElementById('name').value = '';
		document.getElementById('gateway_user').value = '';
		document.getElementById('gateway_password').value = '';
		document.getElementById('url').value = '';
		document.getElementById('preferred').checked = false;
		document.getElementById('voided').checked = false;
	}
	function reset(){
		document.getElementById('gateway_id').value = '';
		document.getElementById('name').value = '';
		document.getElementById('gateway_user').value = '';
		document.getElementById('gateway_password').value = '';
		document.getElementById('url').value = '';
		document.getElementById('preferred').checked = false;
		document.getElementById('voided').checked = false;
	}
</script>

<div>
	<b class="boxHeader">
		<spring:message code="sms.manage_gateway" />
	</b>
	<form method="post">
		<div id="div1">
			<b><spring:message code="sms.current.gateways" /></b>
			<table cellpadding="4">
				<tr>
					<th><u><spring:message code="sms.id"/></u></th>
					<th><u><spring:message code="sms.name"/></u></th>
					<th><u><spring:message code="sms.url"/></u></th>
					<th><u><spring:message code="sms.preferred"/></u></th>
					<th><u><spring:message code="sms.voided"/></u></th>
				</tr>
				<c:forEach items="${providerList}" var="provider" varStatus="varStatus">
					<tr onclick="javascript:selectRow('${provider.id}','${provider.name}','${provider.username}','${provider.password}','${provider.address}',${provider.preferred}, ${provider.voided})"  
						class="<c:choose><c:when test="${varStatus.index % 2 == 0}">evenRow</c:when><c:otherwise>oddRow</c:otherwise></c:choose>">
						<td>${provider.id}</td>
						<td>${provider.name}</td>
						<td>${provider.address}</td>
						<td>${provider.preferred}</td>
						<td>${provider.voided}</td>
					</tr>
				</c:forEach>
			</table>
		</div>
		<div id="div2">
			<b><spring:message code="sms.add.a.new.gateway" /></b>		
			<input id="gateway_id" type="hidden" name="gateway_id" />
			<table>
				<tr>
					<td>
						<spring:message code="sms.name"/> &nbsp; : &nbsp;
					</td>
					<td>
						<input id="name" type="text" size="30" name="name" />
					</td>
				</tr>
				<tr>
					<td>
						<spring:message code="sms.user"/> &nbsp; : &nbsp;
					</td>
					<td>
						<input id="gateway_user" type="text" value=" " size="30" name="gateway_user" />
					</td>
				</tr>
				<tr>
					<td>
						<spring:message code="sms.password"/> &nbsp; : &nbsp;
					</td>
					<td>
						<input id="gateway_password" type="password" value="" size="30" name="gateway_password" />
					</td>
				</tr>
				<tr>
					<td>
						<spring:message code="sms.url"/> &nbsp; : &nbsp;
					</td>
					<td>
						<input id="url" type="text" size="30" name="url" />
					</td>
				</tr>
				<tr>
					<td>
						<spring:message code="sms.preferred"/> &nbsp; : &nbsp;
					</td>
					<td>
						<input id="preferred" type="checkbox" name="preferred" />
					</td>
				</tr>
				<tr>
					<td>
						<spring:message code="sms.voided"/> &nbsp; : &nbsp;
					</td>
					<td>
						<input id="voided" type="checkbox" name="voided" />
					</td>
				</tr>
			</table>	
			<button type="button" onclick="javascript:newGateway();">New</button>
			<input type="submit" value="<spring:message code="general.save"/>"/>
			<button type="button" onclick="javascript:reset();">Reset</button>
		</div>
	</form>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>