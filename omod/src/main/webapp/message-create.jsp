<%@ include file="/WEB-INF/template/include.jsp" %>

<%@ page import = "org.openmrs.module.sms.SmsProgramming, java.util.Map, java.util.HashMap" %>

<openmrs:require privilege="View SMS" otherwise="/login.htm" redirect="/module/sms/message-create.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<div>
	<b class="boxHeader">
		<spring:message code="sms.create_message" />
	</b>
	<div class="box">
		<form method="post">
			<table>
				<tr>
					<td>
						<spring:message code="sms.name"/> &nbsp; : &nbsp;
					</td>
					<td>
						<input type="text" size="52" name="name" />
					</td>
				</tr>
				
				<tr>
					<td>
						<spring:message code="sms.text"/> &nbsp; : &nbsp;
					</td>
					<td>
						<textarea cols="50" rows="5" name="text"> </textarea>
					</td>
				</tr>
				
				<tr>
					<td>
						<spring:message code="sms.send"/>
					</td>
					<td>
						<input type="text" size = "4" name="days_after" />
						 &nbsp; days after &nbsp;
						 <input type="text" size="32" name="event" />
					</td>
				</tr>			
							
				<tr>
					<td>
						<spring:message code="sms.sql"/> &nbsp; : &nbsp;
					</td>
					<td>
						<textarea cols="50" rows="5" name="sql"> </textarea>
					</td>
				</tr>
			
				<tr>
					<td>
						<spring:message code="sms.criteria"/> &nbsp; : &nbsp;
					</td>
					<td>
						<select name="cohort">
							<c:forEach items="${cohortList}" var="cohort">
								<option value="${cohort.id}">${cohort.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
				
				<tr>
					<td>
						<spring:message code="sms.concept"/> &nbsp; : &nbsp;
					</td>
					<td>
						<select name="concept">
							<c:forEach items="${conceptList}" var="concept">
								<option value="${concept.id}">${concept.name.name}</option>
							</c:forEach>
						</select>
					</td>
				</tr>
			</table>
			
			<input type="submit" value="<spring:message code="general.save"/>"/>
		</form>
	</div>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>