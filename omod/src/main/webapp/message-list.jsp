<%@ include file="/WEB-INF/template/include.jsp" %>

<%@ page import = "org.openmrs.module.sms.SmsProgramming, java.util.Map, java.util.HashMap" %>

<openmrs:require privilege="View SMS" otherwise="/login.htm" redirect="/module/sms/message-list.list"/>

<%@ include file="/WEB-INF/template/header.jsp" %>
<%@ include file="localHeader.jsp"%>
<openmrs:htmlInclude file="/scripts/dojo/dojo.js" />
<openmrs:htmlInclude file="/scripts/dojoConfig.js" />

<script type="text/javascript">
var currentIndex = -1;
function editMsg(index){
	if(currentIndex >= 0){
		document.getElementById("msgNameEdit"+currentIndex).style.display = "none";
		document.getElementById("msgDelayEdit"+currentIndex).style.display = "none";
		document.getElementById("msgEventEdit"+currentIndex).style.display = "none";
		document.getElementById("msgCohortEdit"+currentIndex).style.display = "none";
		document.getElementById("msgTextEdit"+currentIndex).style.display = "none";
		document.getElementById("msgSave"+currentIndex).style.display = "none";
		
		document.getElementById("msgNameStatic"+currentIndex).style.display = "";
		document.getElementById("msgDelayStatic"+currentIndex).style.display = "";
		document.getElementById("msgEventStatic"+currentIndex).style.display = "";
		document.getElementById("msgCohortStatic"+currentIndex).style.display = "";
		document.getElementById("msgTextStatic"+currentIndex).style.display = "";
		document.getElementById("msgEdit"+currentIndex).style.display = "";	
	}
	document.getElementById("msgNameEdit"+index).style.display = "";
	document.getElementById("msgDelayEdit"+index).style.display = "";
	document.getElementById("msgEventEdit"+index).style.display = "";
	document.getElementById("msgCohortEdit"+index).style.display = "";
	document.getElementById("msgTextEdit"+index).style.display = "";
	document.getElementById("msgSave"+index).style.display = "";
	
	document.getElementById("msgNameStatic"+index).style.display = "none";
	document.getElementById("msgDelayStatic"+index).style.display = "none";
	document.getElementById("msgEventStatic"+index).style.display = "none";
	document.getElementById("msgCohortStatic"+index).style.display = "none";
	document.getElementById("msgTextStatic"+index).style.display = "none";
	document.getElementById("msgEdit"+index).style.display = "none";	
	currentIndex = index;
}
function save(){
	if(currentIndex >= 0){
		document.getElementById("msgId").value = document.getElementById("msgId"+currentIndex).value; 
		document.getElementById("msgName").value = document.getElementById("msgName"+currentIndex).value;
		document.getElementById("msgDelay").value = document.getElementById("msgDelay"+currentIndex).value;
		document.getElementById("msgEvent").value = document.getElementById("msgEvent"+currentIndex).value;
		document.getElementById("msgCohort").value = document.getElementById("msgCohort"+currentIndex).value;
		document.getElementById("msgText").value = document.getElementById("msgText"+currentIndex).value;
		document.getElementById("form1").submit();
	}
}
</script>

<c:set var="messageListSize" value="${fn:length(messageList)}" />

<div>
	<b class="boxHeader">
		<spring:message code="sms.message_list_programmed" />
	</b>
	<c:choose>
		<c:when test="${messageListSize < 1}">
			<br/>&nbsp;&nbsp;<i>(<spring:message code="sms.null"/>)</i><br/>
		</c:when>
		<c:otherwise>
			<div class="box">
				<form id="form1" method="post">
					<input type="hidden" name="msgId" id="msgId"/>
					<input type="hidden" name="msgName" id="msgName"/>
					<input type="hidden" name="msgDelay" id="msgDelay"/>
					<input type="hidden" name="msgEvent" id="msgEvent"/>
					<input type="hidden" name="msgCohort" id="msgCohort"/>
					<input type="hidden" name="msgText" id="msgText"/>
					<table cellpadding="4">
						<tr>
							<th><u><spring:message code="sms.id"/></u></th>
							<th><u><spring:message code="sms.name"/></u></th>
							<th><u><spring:message code="sms.days.after"/></u></th>
							<th><u><spring:message code="sms.event"/></u></th>
							<th><u><spring:message code="sms.criteria"/></u></th>
							<th><u><spring:message code="sms.text"/></u></th>
							<th><u></u></th>							
						</tr>
						<c:forEach items="${messageList}" var="msg" varStatus="varStatus">
							<tr class="<c:choose><c:when test="${varStatus.index % 2 == 0}">evenRow</c:when><c:otherwise>oddRow</c:otherwise></c:choose>">
								<td>
									<input type="hidden" id="msgId${varStatus.index}" value="${msg.id}"/>
									${msg.id}
								</td>
								<td>
									<div id="msgNameStatic${varStatus.index}">
										${msg.name}
									</div>
									<div  id="msgNameEdit${varStatus.index}" style="display:none">
										<input type="text" id="msgName${varStatus.index}" value="${msg.name}"/>
									</div>
								</td>
								<td>
									<div id="msgDelayStatic${varStatus.index}">
										${msg.delay}
									</div>
									<div  id="msgDelayEdit${varStatus.index}" style="display:none">
										<input type="text" size="5" id="msgDelay${varStatus.index}" value="${msg.delay}"/>
									</div>
								</td>
								<td>
									<div id="msgEventStatic${varStatus.index}">
										${msg.event}
									</div>
									<div  id="msgEventEdit${varStatus.index}" style="display:none">
										<input type="text" size="25" id="msgEvent${varStatus.index}" value="${msg.event}"/>
									</div>
								</td>
								<td>
									<div id="msgCohortStatic${varStatus.index}">
										${msg.cohort}
									</div>
									<div  id="msgCohortEdit${varStatus.index}" style="display:none">
										<input type="text" id="msgCohort${varStatus.index}" value="${msg.cohort}"/>
									</div>
								</td>
								<td>
									<div id="msgTextStatic${varStatus.index}">
										${msg.text}
									</div>
									<div  id="msgTextEdit${varStatus.index}" style="display:none">
										<textarea cols ="75" rows="3" id="msgText${varStatus.index}"/>${msg.text}</textarea>
									</div>
								</td>
								<td>
									<div id="msgEdit${varStatus.index}">
										<a href="javascript:editMsg(${varStatus.index});">Edit</a>
									</div>
									<div id = "msgSave${varStatus.index}" style="display:none">
										<a href="javascript:save();"><spring:message code="general.save"/></a>
									</div>
								</td>
							</tr>
						</c:forEach>
					</table>
				</form>
			</div>
		</c:otherwise>
	</c:choose>
</div>
<br/>
<%@ include file="/WEB-INF/template/footer.jsp" %>