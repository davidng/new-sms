<ul id="menu">
	<li class="first">
		<a href="${pageContext.request.contextPath}/admin"><spring:message code="admin.title.short"/></a>
	</li>

	<openmrs:hasPrivilege privilege="View SMS">
		<li <c:if test='<%= request.getRequestURI().contains("sms/message-test") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/sms/message-test.list">
				<spring:message code="sms.test_message"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="View SMS">
		<li <c:if test='<%= request.getRequestURI().contains("sms/message-list") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/sms/message-list.list">
				<spring:message code="sms.message_list_programmed"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="View SMS">
		<li <c:if test='<%= request.getRequestURI().contains("sms/message-create") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/sms/message-create.list">
				<spring:message code="sms.create_message"/>
			</a>
		</li>
	</openmrs:hasPrivilege>

	<openmrs:hasPrivilege privilege="View SMS">
		<li <c:if test='<%= request.getRequestURI().contains("sms/gateways") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/sms/gateways.list">
				<spring:message code="sms.manage_gateway"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
	
	<openmrs:hasPrivilege privilege="View SMS">
		<li <c:if test='<%= request.getRequestURI().contains("sms/message-queue") %>'>class="active"</c:if>>
			<a href="${pageContext.request.contextPath}/module/sms/message-queue.list">
				<spring:message code="sms.message_queue"/>
			</a>
		</li>
	</openmrs:hasPrivilege>
</ul>