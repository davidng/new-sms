package org.openmrs.module.sms.schedule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.send.SendSms;
import org.openmrs.scheduler.tasks.AbstractTask;

public class SmsSendingTask extends AbstractTask{

	private Log log = LogFactory.getLog(SmsSendingTask.class);
	private static Boolean isRunning = false;
	@Override
	public void execute() {
		synchronized (isRunning){
			if (isRunning) {
				log.warn("Sms sending processor aborting (another processor already running)");
				return;
			}
			isRunning = true;
		}
		try{
			log.info("Starting Automated task for sms sending..");
			
			Context.openSession();
			log.info("Login to OpenMRS");
			Context.getAdministrationService();			
			log.error("Sending out message ... ");
			SendSms.sendMessage();
			Context.closeSession();
		}catch(Exception e){
			log.error("Exception in SmsSendingTask", e);
		}
		isRunning = false;
	}

}
