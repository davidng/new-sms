package org.openmrs.module.sms.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.AdministrationService;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.send.SendSms;
import org.openmrs.module.sms.util.SMSConstants;

public class EmailGateway {
	protected static Log log;
	private Session session;

	public void startup() {
		log = LogFactory.getLog(SendSms.class);
		final AdministrationService adminService = Context.getAdministrationService();
		Properties props = new Properties();
		props.setProperty(SMSConstants.GP_MAIL_PROTOCOL,
				adminService.getGlobalProperty(SMSConstants.GP_MAIL_PROTOCOL_VALUE));
	    props.setProperty(SMSConstants.GP_MAIL_STARTTLS_ENABLE, 
	    		adminService.getGlobalProperty(SMSConstants.GP_MAIL_STARTTLS_ENABLE_VALUE));
	    props.setProperty(SMSConstants.GP_MAIL_HOST,
	    		adminService.getGlobalProperty(SMSConstants.GP_MAIL_HOST_VALUE));
	    props.setProperty(SMSConstants.GP_MAIL_PORT, 
	    		adminService.getGlobalProperty(SMSConstants.GP_MAIL_PORT_VALUE));
	    props.setProperty(SMSConstants.GP_MAIL_AUTH,
	    		adminService.getGlobalProperty(SMSConstants.GP_MAIL_AUTH_VALUE));
	    props.setProperty(SMSConstants.GP_MAIL_FROM,
	    		adminService.getGlobalProperty(SMSConstants.GP_MAIL_FROM_VALUE));
	    
	    Authenticator auth = new Authenticator() {

			@Override
			public PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(
						adminService.getGlobalProperty(SMSConstants.GP_MAIL_USER), 
						adminService.getGlobalProperty(SMSConstants.GP_MAIL_PASSWORD));
			}
		};
		session = Session.getInstance(props, auth);
		log.error("started gate way successfully");
	}

	public boolean sendMessage(String subject, String content, String recipients) {
	
		try {
		    MimeMessage message = new MimeMessage(session);
		    message.addRecipient(Message.RecipientType.TO, getToAddresses(recipients));
		    message.setSubject(subject);
		    message.setText(content);
		    Transport.send(message);
		    log.debug("message sent successfully");
		    return true;
		} catch (MessagingException e) {
			//check errors and act accordingly
			log.error("Error sending message: " + e.getMessage());
			SMSConstants.errorMessage=e.getMessage();
		}
		return false;
	}

	private  static InternetAddress getToAddresses(String address) {
		InternetAddress toAddress = new InternetAddress();
		
	    // To get the array of addresses
		try {
			toAddress = new InternetAddress(address);
		} catch (AddressException e) {
			log.error("Error converting address: " + address + " the error is: " + e.getMessage());
		}
		return toAddress;
	}
	
	public boolean canSend() {
		return session != null;
	}
	
	public void shutdown() {
		log.error("shutting down gateway");
		session = null;
	}
}