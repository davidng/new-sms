/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */

package org.openmrs.module.sms.api.db;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.openmrs.Patient;
import org.openmrs.module.sms.SmsHistory;
import org.openmrs.module.sms.SmsProgramming;
import org.openmrs.module.sms.SmsProvider;
import org.openmrs.module.sms.SmsQueue;

public interface SmsModuleDAO {

	public void setSessionFactory(SessionFactory session);
	
	public SmsProvider createProvider(SmsProvider provider);
	
	public void deleteProvider(SmsProvider provider);
	
	public List<SmsProvider> getAllProviders();
	
	
	public List<SmsProvider> getActiveProviders();
	
	public List<SmsProvider> getPreferredProvider();
	
	public SmsProvider getProviderById(Integer id);
	
	public List<SmsProgramming> getAllMessages();
	
	public SmsProgramming getMessageById(Integer id);
	
	public List<SmsProgramming> getMessagesByKeyWord(String key);
	
	public SmsProgramming createMessage(SmsProgramming programming);
		
	public void deteleMessage(SmsProgramming programming);
	
	public void createHistory(SmsHistory history);

	public SmsHistory getHistory(Patient patient, SmsProgramming programming);

	public List<SmsHistory> getListHistory(Patient patient, SmsProgramming programming);
        
    public List<SmsHistory> getListHistory(SmsProgramming programming);
	
	public List<Integer> getListObservationFromConceptAndPatient(Patient patient, SmsProgramming programming);
        
    public List getListObservationFromConcept(SmsProgramming programming);
    
    public List getListObservationFromProgramming(SmsProgramming programming, List<Integer> listPatient, boolean sendRepeat);

	public Date getDateSQL(String sql, Integer patientId);	
	
	public List<SmsQueue> getAllSmsQueue();
	
	public void saveSmsQueue(SmsQueue smsQueue);
	
	public void deleteSmsQueue(SmsQueue smsQueue);
	
	public SmsQueue getSmsQueueById(Integer id);
	
	public void deleteAllSmsQueue();
}
