/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.api.impl;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.Patient;
import org.openmrs.api.impl.BaseOpenmrsService;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.api.db.SmsModuleDAO;
import org.openmrs.module.sms.SmsHistory;
import org.openmrs.module.sms.SmsProgramming;
import org.openmrs.module.sms.SmsProvider;
import org.openmrs.module.sms.SmsQueue;
import org.springframework.stereotype.Component;

/**
 * @author crecabarren
 *
 */
@Component
public class SmsServiceImpl extends BaseOpenmrsService implements SmsService {
	protected final Log log = LogFactory.getLog(SmsServiceImpl.class);
	private SmsModuleDAO dao;
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#setSmsDAO(org.openmrs.module.sms.db.SmsDAO)
	 */
	public void setSmsDAO(SmsModuleDAO dao) {
		this.dao = dao;
	}
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#createProvider(org.openmrs.module.sms.model.SmsProvider)
	 */
	public SmsProvider createProvider(SmsProvider provider) {
		return dao.createProvider(provider);
	}

	/**
	 * @see org.openmrs.module.sms.api.SmsService#deleteProvider(org.openmrs.module.sms.model.SmsProvider)
	 */
	public void deleteProvider(SmsProvider provider) {
		dao.deleteProvider(provider);
	}

	/**
	 * @see org.openmrs.module.sms.api.SmsService#getAllProviders()
	 */
	public List<SmsProvider> getAllProviders() {
		return dao.getAllProviders();
	}
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getActiveProviders()
	 */
	public List<SmsProvider> getActiveProviders(){
		return dao.getActiveProviders();
	}

	/**
	 * @see org.openmrs.module.sms.api.SmsService#getPreferredProvider()
	 */
	public List<SmsProvider> getPreferredProvider() {
		return dao.getPreferredProvider();
	}

	/**
	 * @see org.openmrs.module.sms.api.SmsService#getProviderById(java.lang.String)
	 */
	public SmsProvider getProviderById(Integer id) {
		return dao.getProviderById(id);
	}
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getAllMessages()
	 */
	public List<SmsProgramming> getAllMessages(){
		return dao.getAllMessages();
	}
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getMessageById(java.lang.Integer)
	 */
	public SmsProgramming getMessageById(Integer id){
		return dao.getMessageById(id);
	}
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#getMessagesByKeyWord(java.lang.String)
	 */
	public List<SmsProgramming> getMessagesByKeyWord(String key){
		return dao.getMessagesByKeyWord(key);
	}
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#createMessage(org.openmrs.module.sms.model.SmsProgramming)
	 */
	public SmsProgramming createMessage(SmsProgramming programming){
		return dao.createMessage(programming);
	}
	
	/**
	 * @see org.openmrs.module.sms.api.SmsService#deteleMessage(org.openmrs.module.sms.model.SmsProgramming)
	 */
	public void deteleMessage(SmsProgramming programming){
		dao.deteleMessage(programming);
	}
	
	/**
	 * 
	 * @see org.openmrs.module.sms.api.SmsService#createHistory(org.openmrs.module.sms.model.SmsHistory)
	 */
	public void createHistory(SmsHistory history){
		dao.createHistory(history);
	}

	/**
	 * 
	 * @see org.openmrs.module.sms.api.SmsService#createHistory(org.openmrs.module.sms.model.SmsHistory)
	 */
	public SmsHistory getHistory(Patient patient, SmsProgramming programming){
		return dao.getHistory(patient, programming);
	}
	public List<SmsHistory> getListHistory(Patient patient, SmsProgramming programming){
		return dao.getListHistory(patient, programming);
	}
        
        public List<SmsHistory> getListHistory(SmsProgramming programming){
                return dao.getListHistory(programming);
        }
        
	public List<Integer> getListObservationFromConceptAndPatient(Patient patient, SmsProgramming programming){
		return dao.getListObservationFromConceptAndPatient(patient, programming);
	}
        
    public List getListObservationFromConcept(SmsProgramming programming){
		return dao.getListObservationFromConcept(programming);
	}
    
    public List getListObservationFromProgramming(SmsProgramming programming, List<Integer> listPatient, boolean sendRepeat){
    	return dao.getListObservationFromProgramming(programming, listPatient, sendRepeat);
    }
        
	/**
	 * @param patient
	 * @param sms
	 * @param coderr
	 * @param deserr
	 */
	public SmsHistory createSmsHistory(Patient patient, SmsProgramming sms,
			String coderr, String deserr, Integer obs_id) {
		SmsHistory history = new SmsHistory();
		history.setPatientId(patient.getPatientId());
		history.setProgrammingId(sms.getId());
		
		//Adding code for multi observation
		history.setObsId(obs_id);
		SmsProvider provider = dao.getProviderById(1);
		List<SmsProvider> providers = getPreferredProvider();
		if (providers != null && providers.size() > 0){
			provider = providers.get((int)Math.random() * providers.size());
		} else{
			//TODO: todo here
			log.error("There must be at least one provider entered");
		}
		
		history.setProviderId(provider.getId());
		history.setSentToGatewayDate(new Timestamp(Calendar.getInstance().getTimeInMillis()) );
		history.setSentToGatewayResult("sending");
		history.setSmsSentDate(new Timestamp(Calendar.getInstance().getTimeInMillis()));
		history.setSmsSentResultCode(coderr);
		history.setSmsSentResult(deserr);
		
		createHistory(history);
		
		return history;
	}

	@Override
	public Date getDateSQL(String sql, Integer patientId) {
		if (sql != null && !sql.isEmpty()){
			String sqlEscapeComment = sql.replaceAll("#\\w+\\r\\n", "");
			return dao.getDateSQL(sqlEscapeComment, patientId);
		}
		
		return null;
	}
	
	@Override
	public List<SmsQueue> getAllSmsQueue(){
		return dao.getAllSmsQueue();
	}
	
	@Override
	public void saveSmsQueue(SmsQueue smsQueue){
		dao.saveSmsQueue(smsQueue);
	}
	
	@Override
	public void deleteSmsQueue(SmsQueue smsQueue){
		log.info("Delete smsQueue in SmsServiceImpl id: " + smsQueue.getId());
		dao.deleteSmsQueue(smsQueue);
	}
	
	public SmsQueue getSmsQueueById(Integer id){
		return dao.getSmsQueueById(id);
	}
	
	public void deleteAllSmsQueue(){
		dao.deleteAllSmsQueue();
	}
}
