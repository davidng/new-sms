package org.openmrs.module.sms;

import java.io.Serializable;
import java.sql.Timestamp;

import org.openmrs.BaseOpenmrsObject;
import org.openmrs.Patient;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.api.SmsService;

public class SmsQueue extends BaseOpenmrsObject implements Serializable
{
	private static final long serialVersionUID = 2362717782133670884L;
	private Integer id;
	private SmsProgramming programming;
	private Patient patient;
	private Integer numberAttempt = 0;
	private Timestamp lastAttemptDateTime;
	private Integer obsId;

	public Integer getObsId() {
		return obsId;
	}

	public void setObsId(Integer obsId) {
		this.obsId = obsId;
	}

	@Override
	public Integer getId() {
		return id;
	}

	@Override
	public void setId(Integer id) {
		this.id = id;
		
	}

	public SmsProgramming getProgramming() {
		return programming;
	}

	public void setProgramming(SmsProgramming programming) {
		this.programming = programming;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Integer getNumberAttempt() {
		return numberAttempt;
	}

	public void setNumberAttempt(Integer numberAttempt) {
		this.numberAttempt = numberAttempt;
	}

	public Timestamp getLastAttemptDateTime() {
		return lastAttemptDateTime;
	}

	public void setLastAttemptDateTime(Timestamp lastAttemptDateTime) {
		this.lastAttemptDateTime = lastAttemptDateTime;
	}
}
