/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.openmrs.BaseOpenmrsObject;

/**
 * A Class to store sms and features to trigger the send of them.
 * 
 * @author crecabarren
 * @since 0.1
 */
public class SmsProgramming extends BaseOpenmrsObject implements Serializable {
	
	private static final long serialVersionUID = 2383643578364538745L;
	private Integer id;
	private String name;
	private String text;
	private Integer delay;
	private String event;
	private Integer cohort;
	private Integer concept;
	private Boolean voided;
	private Timestamp voidedDate;
	private Integer voidedBy;
	private String eventSQL;
	
	/**
	 * @return the voided
	 */
	public Boolean getVoided() {
		return voided;
	}
	/**
	 * @param voided the voided to set
	 */
	public void setVoided(Boolean voided) {
		this.voided = voided;
	}
	/**
	 * @return the voidedDate
	 */
	public Timestamp getVoidedDate() {
		return voidedDate;
	}
	/**
	 * @param voidedDate the voidedDate to set
	 */
	public void setVoidedDate(Timestamp voidedDate) {
		this.voidedDate = voidedDate;
	}
	/**
	 * @return the voidedBy
	 */
	public Integer getVoidedBy() {
		return voidedBy;
	}
	/**
	 * @param voidedBy the voidedBy to set
	 */
	public void setVoidedBy(Integer voidedBy) {
		this.voidedBy = voidedBy;
	}
	/**
	 * @see org.openmrs.OpenmrsObject#getId()
	 */
	public Integer getConcept() {
		return concept;
	}
	/**
	 * @param concept the concept to set
	 */
	public void setConcept(Integer concept) {
		this.concept = concept;
	}
	/**
	 * @see org.openmrs.OpenmrsObject#getId()
	 */
	public Integer getId() {
		return this.id;
	}
	/**
	 * @see org.openmrs.OpenmrsObject#setId(java.lang.Integer)
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}
	/**
	 * @return the delay
	 */
	public Integer getDelay() {
		return delay;
	}
	/**
	 * @param delay the delay to set
	 */
	public void setDelay(Integer delay) {
		this.delay = delay;
	}
	/**
	 * @return the event
	 */
	public String getEvent() {
		return event;
	}
	/**
	 * @param event the event to set
	 */
	public void setEvent(String event) {
		this.event = event;
	}
	/**
	 * @return the cohort
	 */
	public Integer getCohort() {
		return cohort;
	}
	/**
	 * @param cohort the cohort to set
	 */
	public void setCohort(Integer cohort) {
		this.cohort = cohort;
	}	
	public String getEventSQL() {
		return eventSQL;
	}
	public void setEventSQL(String eventSQL) {
		this.eventSQL = eventSQL;
	}
}
