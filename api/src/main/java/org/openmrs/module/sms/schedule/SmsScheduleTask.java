/**
 * Copyright (C) 2009-2010 eHealth Systems Ltd. All Rights Reserved.
 *
 * Reproduction or distribution of this source code is prohibited.
 */
package org.openmrs.module.sms.schedule;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openmrs.api.context.Context;
import org.openmrs.module.sms.api.SmsService;
import org.openmrs.module.sms.SmsProgramming;
import org.openmrs.module.sms.send.SendSms;
import org.openmrs.scheduler.tasks.AbstractTask;

/**
 * A Class to provide an interface for schedule task in openmrs scheduler.
 * @author crecabarren
 * @since 0.1
 */
public class SmsScheduleTask extends AbstractTask {

	private SmsService service;
	private Log log = LogFactory.getLog(SmsScheduleTask.class);
	private static Boolean isRunning = false;
	
	/**
	 * @see org.openmrs.scheduler.tasks.AbstractTask#execute()
	 */
	@Override
	public void execute() {
		synchronized (isRunning){
			if (isRunning) {
				log.warn("Sms preparing processor aborting (another processor already running)");
				return;
			}
			isRunning = true;
		}
		try{
			log.info("Starting Automated task for preparing sms ..");
			
			List<SmsProgramming> messages;
			//trying to login into OpenMRS instance
			Context.openSession();
	//		if (!Context.isAuthenticated()){
	//			authenticate();
	//		}
			log.info("Login to OpenMRS");
	//		Context.authenticate("scheduler", "Salude123"); this is a daemon thread, so don't need to authenticate
			Context.getAdministrationService();
			
			service = Context.getService(SmsService.class);
			//log.error("deleting all old sms queue");
			//service.deleteAllSmsQueue();
			messages = service.getAllMessages();
			log.error("getting all message in Task");
			
			for (SmsProgramming p : messages){
				log.error("Loading messages in: "+p.getName());
				try {
					SendSms.prepareMessage(p);
				} catch (Exception e){
					e.printStackTrace();
				}
			}
			Context.closeSession();
		}catch(Exception e){
			log.error("Exception in SmsScheduleTask", e);
		}
		isRunning = false;
	}
	
}
